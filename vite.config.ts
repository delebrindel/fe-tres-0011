import * as path from 'path';
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), tsconfigPaths()],
  resolve: {
    alias: {
      src: path.resolve('src/'),
      scss: path.resolve('src/scss/'),
      sectionScss: path.resolve('src/scss/sections/'),
    },
  }
  //base: '/fe-tres-0011/'
})
