export const BASE_PORTFOLIO_URL = '/assets/img/projects/';

export const OUR_PORTFOLIO = [
  {
    id: 1,
    name: "Raffoom Digital Website",
    year: 2021,
    url: "https://www.raffoom.com/",
    description:
      ['Webpage for a raffling startup'],
    img: `${BASE_PORTFOLIO_URL}raffoom.png`,
    imgMobile: `${BASE_PORTFOLIO_URL}raffoom-m.png`,
    technologies: ['React', 'SCSS']
  },
  {
    id: 3,
    name: "Farmaenvios",
    year: 2021,
    url: "https://farmaenvios.com/",
    img: `${BASE_PORTFOLIO_URL}farma.png`,
    imgMobile: `${BASE_PORTFOLIO_URL}farma-m.png`,
    description:[
      'Webpage for a drugstore delivery service',
    ],
    technologies: ['React', 'PHP', 'SCSS']
  },
  {
    id: 2,
    name: "Sanders Y Mantilla, Traductores",
    year: 2020,
    url: "https://sandersymantilla.com/",
    img: `${BASE_PORTFOLIO_URL}sandm.png`,
    imgMobile: `${BASE_PORTFOLIO_URL}sandm-m.png`,
    description:[
      'Webpage for a translation agency',
      'Features multilanguage support and contact form.',
    ],
    technologies: ['React', 'PHP', 'SCSS']
  },
  {
    id: 4,
    name: "Plapers",
    year: 2019,
    url: "https://plapers.com.mx/",
    img: `${BASE_PORTFOLIO_URL}plapers.png`,
    imgMobile: `${BASE_PORTFOLIO_URL}plapers-m.png`,
    description:[
      'Webpage for custom made license plates',
      'Features full preview of how license plates will look when manufactured',
    ],
    technologies: ['Vue', 'PHP', 'SCSS']
  },
];
