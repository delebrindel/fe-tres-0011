export enum ROUTE_NAMES {
  ABOUT_US = 'about',
  PORTFOLIO = 'portfolio',
  CONTACT = 'contact'
}

export enum ROUTES {
  ABOUT_US = '#about',
  CONTACT = '#contact',
  PORTFOLIO = '#portfolio',
}
export enum SECTION_NAMES {
  ABOUT_US = 'About Us',
  CONTACT = 'Get In Touch',
  PORTFOLIO = 'Our Portfolio',
}

export const SUPPORT_EMAIL = 'dev@tres0011.com';

export const MENU_LINKS = [
  {link: ROUTES.ABOUT_US, text: SECTION_NAMES.ABOUT_US},
  {link: ROUTES.PORTFOLIO, text: SECTION_NAMES.PORTFOLIO},
  {link: ROUTES.CONTACT, text: SECTION_NAMES.CONTACT},
]