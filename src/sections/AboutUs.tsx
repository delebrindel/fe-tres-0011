import { FC } from "react";
import { ROUTE_NAMES, SECTION_NAMES } from "src/enums/Links";

export const AboutUs: FC = () => {
  return (
    <section id={ROUTE_NAMES.ABOUT_US} className="wrapper column dark slanted">
      <div className="content">
        <h1>{SECTION_NAMES.ABOUT_US}</h1>
        <div className="row spaceBetween">
          <img src="assets/img/monsters.svg" alt="About Us" />
          <p>
            <small>
              We are a passionate couple of a Developer and a Designer looking
              to help you with any code needs you might have.
              <br />
              With 11 years of experience working for different customers around
              the world we are confident in bringing you a custom-tailored
              solution for your specific use case.
            </small>
          </p>
        </div>
      </div>
    </section>
  );
};
