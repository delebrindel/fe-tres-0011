import { Portfolio } from "@components/Portfolio/Portfolio";
import { FC } from "react";
import { ROUTE_NAMES, SECTION_NAMES } from "src/enums/Links";

export const OurPortfolio: FC = () => {
  return (
    <section id={ROUTE_NAMES.PORTFOLIO} className='wrapper column afterSlanted'>
      <h1>{SECTION_NAMES.PORTFOLIO}</h1>
      <Portfolio />
    </section>
  );
};
