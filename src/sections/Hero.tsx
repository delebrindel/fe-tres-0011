import { FC } from "react";

export const Hero: FC = () => {
  return (
    <section id="hero" className="wrapper column light py10">

      <div className="row spaceBetween">
        <img
          src="/assets/img/progressive_app.svg"
          alt="Software development"
        />
        <p>
          Bespoke solutions for <strong>you</strong>
          <br />
          and <strong>your business</strong>
        </p>
      </div>
    </section>
  );
};
