import { SimpleHero } from "@components/SimpleHero";
import { FC } from "react";
import { ROUTE_NAMES, SECTION_NAMES, SUPPORT_EMAIL } from "src/enums/Links";

export const Contact: FC = () => {
  return (
    <section className="wrapper dark column" id={ROUTE_NAMES.CONTACT}>
      <div className="content">
        <SimpleHero title={SECTION_NAMES.CONTACT}>
          <>
            <p>Drop us a line at</p>
            <p>
              <a href={`mailto: ${SUPPORT_EMAIL}`}>Our Dev Email</a>
            </p>
          </>
        </SimpleHero>
      </div>
    </section>
  );
};
