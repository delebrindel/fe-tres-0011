import { FC, useState } from "react";
import { OUR_PORTFOLIO } from "src/enums/Portfolio";
import portfolioStyles from "./Portfolio.module.scss";

type ProjectData = {
  name: string;
  img: string;
  imgMobile: string;
  description: string[];
  url: string;
};

type PortfolioItemProps = {
  portfolioData: ProjectData;
  onImageClick: () => void;
};

type PortfolioModalProps = {
  activeItem: number;
  onClose: () => void;
};

const PortfolioModal: FC<PortfolioModalProps> = ({ activeItem, onClose }) => {
  const ProjectData =
    OUR_PORTFOLIO.find((item) => item.id === activeItem) ?? null;

  return (
    <>
      {ProjectData && (
        <div className={portfolioStyles.modalBackground} onClick={onClose}>
          <p className={portfolioStyles.lead}>{ProjectData.name}</p>
          <img className={portfolioStyles.preview} src={ProjectData.img} alt={ProjectData.name} />
          <img className={portfolioStyles.previewMobile} src={ProjectData.imgMobile} alt={ProjectData.name} />
          {ProjectData.description.map((item, iteration) => (
            <p key={iteration}>{item}</p>
          ))}
          <a target='portfolio' className={portfolioStyles.link} href={ProjectData.url}>{ProjectData.url}</a>
          <small>{ProjectData.year}</small>
        </div>  
      )}
    </>
  );
};

const PortfolioItem: FC<PortfolioItemProps> = ({
  portfolioData,
  onImageClick,
}) => {
  const { name, img, url } = portfolioData;

  return (
    <div className={portfolioStyles.item} onClick={onImageClick}>
      <h3>{name}</h3>
      <img src={img} alt={name} />
      <a href={url} target="portfolio">
        {url}
      </a>
    </div>
  );
};

export const Portfolio: FC = () => {
  const [activeProject, setActiveProject] = useState<number>(0);

  return (
    <>
      {activeProject !== 0 && (
        <PortfolioModal
          activeItem={activeProject}
          onClose={() => {
            setActiveProject(0);
          }}
        />
      )}
      <div className={portfolioStyles.wrapper}>
        {OUR_PORTFOLIO.map((portfolioItem) => (
          <PortfolioItem
            key={portfolioItem.id}
            portfolioData={portfolioItem}
            onImageClick={() => {
              setActiveProject(portfolioItem.id);
            }}
          />
        ))}
      </div>
    </>
  );
};
