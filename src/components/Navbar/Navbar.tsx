import { FC, useCallback, useState } from "react";
import NavbarStyles from "./Navbar.module.scss";
import { Icon } from "@iconify/react";
import { MENU_LINKS, ROUTES } from "src/enums/Links";

export const Navbar: FC = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const closeMenu = useCallback(() => {
    setMenuOpen(false);
  }, []);

  return (
    <nav className={NavbarStyles.container}>
      <div className={NavbarStyles.navbarWrapper}>
        <div className={NavbarStyles.logo}>
            <a href='#' onClick={closeMenu}>
              Tres <strong>0011</strong>
            </a>
          <div
            onClick={() => {
              setMenuOpen((lastState) => !lastState);
            }}
            className={NavbarStyles.menuControl}
          >
            {menuOpen ? <Icon icon="ep:close-bold" /> : <Icon icon="ant-design:menu-outlined" />}
          </div>
        </div>
        <ul className={menuOpen ? "" : NavbarStyles.hideMenu}>
          {MENU_LINKS.map(menuItem => <li key={menuItem.link} onClick={closeMenu}>
            <a href={menuItem.link}>{menuItem.text}</a>
          </li>)}
        </ul>
      </div>
    </nav>
  );
};
