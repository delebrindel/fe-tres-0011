import { FC } from "react";
import FooterStyles from "./Footer.module.scss";
import { Icon } from "@iconify/react";

export const Footer: FC = () => {
  return (
    <footer className={FooterStyles.footerWrapper}>
      Powered by{" "}
      <a href="https://vitejs.dev/" target='poweredBy'>
        <Icon icon="simple-icons:vite" className={FooterStyles.icon} />
      </a>
      <a href="https://gitlab.com/delebrindel/fe-tres-0011"target='poweredBy'>
        <Icon icon="simple-icons:gitlab" className={FooterStyles.icon} />
      </a>
      &nbsp;&nbsp;&nbsp; <a href="https://undraw.co" target='poweredBy'>Undraw.co</a>
    </footer>
  );
};
