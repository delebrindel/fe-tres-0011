import { FC } from "react";
type SimpleHeroProps = {
  title: string;
  children: JSX.Element;
};

export const SimpleHero: FC<SimpleHeroProps> = ({ title, children }) => {
  return (
    <>
      <h1>{title}</h1>
      {children}
    </>
  );
};
