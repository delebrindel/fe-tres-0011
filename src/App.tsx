import { AboutUs } from "./sections/AboutUs";
import { Hero } from "./sections/Hero";
import { Navbar } from "./components/Navbar/Navbar";
import { OurPortfolio } from "./sections/OurPortfolio";
import { Footer } from "@components/Footer/Footer";
import { Contact } from "./sections/Contact";

function App() {

  return (
    <main className="App">
      <Navbar />
      <Hero />
      <AboutUs />
      <OurPortfolio />
      <Contact />
      <Footer />
    </main>
  );
}

export default App;
